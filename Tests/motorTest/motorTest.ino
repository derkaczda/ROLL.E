#include <Stepper.h>

#define STEPS_PER_REVOLUTION 200
#define MOTOR_MAX_SPEED 100
#define MOTOR_SPEED 50

#define MOTOR_RIGHT_PIN_1 3
#define MOTOR_RIGHT_PIN_2 4
#define MOTOR_RIGHT_PIN_3 5
#define MOTOR_RIGHT_PIN_4 6

#define MOTOR_LEFT_PIN_1 8
#define MOTOR_LEFT_PIN_2 9
#define MOTOR_LEFT_PIN_3 10
#define MOTOR_LEFT_PIN_4 11


/*
 * Variablen
 */
 Stepper motorLeft(STEPS_PER_REVOLUTION, MOTOR_LEFT_PIN_1, MOTOR_LEFT_PIN_2, MOTOR_LEFT_PIN_3, MOTOR_LEFT_PIN_4);
 Stepper motorRight(STEPS_PER_REVOLUTION, MOTOR_RIGHT_PIN_1, MOTOR_RIGHT_PIN_2, MOTOR_RIGHT_PIN_3, MOTOR_RIGHT_PIN_4);
 

void setup() {
  
  motorLeft.setSpeed(MOTOR_SPEED); //max 100
  motorRight.setSpeed(MOTOR_SPEED);
  //anfahren();
}

void anfahren()
{
  for(int i = 10; i < 60; i += 10)
  {
    motorLeft.setSpeed(i);
    motorRight.setSpeed(i);
    for(int i = 0; i < STEPS_PER_REVOLUTION; i++)
    {
    motorLeft.step(1);
    motorRight.step(1);
    }
  }
}

void loop() 
{
  
  drive();
  
}

void drive()
{
  for(int i = 0; i < STEPS_PER_REVOLUTION; i++)
  {
    motorLeft.step(1);
    motorRight.step(1);
  }
}

